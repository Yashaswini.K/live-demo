import React, { useEffect } from "react";
import HeaderComponent from "../HeaderComponent/HeaderComponent";
import "./Pricing.css";
import play from "../../assets/Footer/play.png";
import pricingimg from "../../assets/pricing/pricing-02-01.png";
import avatar2 from "../../assets/pricing/avatar2.svg";
import listTick from "../../assets/pricing/Path 406.svg";
import groupavatar from "../../assets/pricing/groupavatar.svg";
import business from "../../assets/pricing/business.svg";
import pin from "../../assets/pricing/push_pin_black_24dp.svg";
import FooterComponent from "../FooterComponent/FooterComponent";
import { Link } from "react-router-dom";
import Tick from "../../assets/pricing/tick.png";
import Cross from "../../assets/pricing/cross.png";

const Pricing = ({ setActivePage, activePage }) => {
  useEffect(() => {
    setActivePage("pricing");
    return () => {
      setActivePage();
    };
  }, []);
  return (
    <div style={{ overflowX: "hidden" }}>
      {/* Main Section Starts Here*/}
      <div className="pricing-bg position-relative">
        <HeaderComponent activePage={activePage} />
        <div className=" mx-5">
          <div className="row px-3">
            <div className="col-lg-6 mt-2">
              <div className="px-3 mt-5">
                <h2 className="pricing-heeader">
                  Agile quality unified across your testing needs
                </h2>

                <div className="textbg text-left">
                  <p className="pricing-subheader mt-5">
                    Cloud and On-Premise offering
                  </p>
                  <div className="row align-items-center mt-5">
                    <div className=" col-6 col-md-6 col-lg-6  h-100">
                      <button className="btn  reqdemobtn ">
                        <Link
                          className=""
                          to="/requestdemo"
                          style={{ textDecoration: "none" }}
                        >
                          <p className="m-0 text-white demorequestdemo">
                            {" "}
                            REQUEST A DEMO
                          </p>
                        </Link>
                      </button>
                    </div>
                    <div className="col-6 col-md-6  col-lg-6  h-100 ">
                      <img src={play} className="homePlayBtn d-inline" alt="" />
                      <h6 className="homePlayText d-inline text-white">
                        WATCH THE VIDEO
                      </h6>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-6  mt-2 carouseldiv px-0 ">
              <div className="my-3 carouselchildiv">
                <img className="pricing-img" src={pricingimg}></img>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* Main Section End Here */}
      {/* Menu Tabs Starts Here */}
      <div className="tab-content tab_content_bg pt-4 ">
        <div
          className=" w-100 px-3 position-relative"
          style={{ top: "-100px", zIndex: "1" }}
        >
          <div className="row">
            <div className="col-md-4 col-sm-6 col-12 card shadow col-12 mx-auto col-md-4 mt-5 mb-1 pricinglongcard">
              <div>
                <div className="card-body ">
                  <div style={{ minHeight: "80px " }}>
                    <img className="w-25 my-2" src={avatar2}></img>
                  </div>
                  <h5 className="card-title pricingcardtitle">Cloud Basic</h5>
                  <p className="card-text pricingcardtext">
                    For small teams trying out Live Demo for a year
                  </p>
                  <p className="pricingcardpricetext my-4">It's Free!</p>
                  <div className="pricingcardbtn  w-100 mx-auto py-2 text-white">
                    Get Started
                  </div>
                  <div className="w-100 my-3">
                    <div className="d-flex align-items-start py-1">
                      <img
                        className=" float-left pl-1 pt-1 pricingcardimg"
                        src={listTick}
                      ></img>
                      <span className="pl-3  pricingcardpoints">
                        Up to 25 Pages
                      </span>
                    </div>
                    <div className="d-flex align-items-start py-1">
                      <img
                        className=" float-left pl-1 pt-1 pricingcardimg"
                        src={listTick}
                      ></img>
                      <span className="pl-3  pricingcardpoints">
                        Up to 50 Elements per Page
                      </span>
                    </div>
                    <div className="d-flex align-items-start py-1">
                      <img
                        className=" float-left pl-1 pt-1 pricingcardimg"
                        src={listTick}
                      ></img>
                      <span className="pl-3  pricingcardpoints">
                        Up to 50 Program Elements
                      </span>
                    </div>
                    <div className="d-flex align-items-start py-1">
                      <img
                        className=" float-left pl-1 pt-1 pricingcardimg"
                        src={listTick}
                      ></img>
                      <span className="pl-3  pricingcardpoints">
                        Up to 25 Step Groups
                      </span>
                    </div>
                    <div className="d-flex align-items-start py-1">
                      <img
                        className=" float-left pl-1 pt-1 pricingcardimg"
                        src={listTick}
                      ></img>
                      <span className="pl-3  pricingcardpoints">
                        Web Applications Automated Testing
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-4 col-sm-6 col-12 card shadow col-12 mx-auto col-md-4 mt-5 mb-1 pricinglongcard">
              <div>
                <div className="card-body ">
                  <div style={{ minHeight: "80px " }}>
                    <img className="w-25 my-2" src={groupavatar}></img>
                  </div>
                  <h5 className="card-title pricingcardtitle">
                    Cloud Professional
                  </h5>
                  <p className="card-text pricingcardtext">
                    For small and medium sized businesses
                  </p>
                  <p className="pricingcardpricetext my-4">$ 95 / Month</p>
                  <div className="pricingcardbtn  w-100 mx-auto py-2 text-white">
                    Get Started
                  </div>
                  <div className="w-100 my-3 pricingcardpointdiv">
                    <div className="d-flex align-items-start py-1">
                      <img
                        className=" float-left pl-1 pt-1 pricingcardimg"
                        src={listTick}
                      ></img>
                      <span className="pl-3  pricingcardpoints">
                        Access to Web, Mobile, Web & Mobile, Web Services and
                        SFDC Automation
                      </span>
                    </div>
                    <div className="d-flex align-items-start py-1">
                      <img
                        className=" float-left pl-1 pt-1 pricingcardimg"
                        src={listTick}
                      ></img>
                      <span className="pl-3  pricingcardpoints">
                        Access to all the features of the product
                      </span>
                    </div>
                    <div className="d-flex align-items-start py-1">
                      <img
                        className=" float-left pl-1 pt-1 pricingcardimg"
                        src={listTick}
                      ></img>
                      <span className="pl-3  pricingcardpoints">
                        No limitations for Test Script, Pages, Elements, Program
                        Elements and Step groups
                      </span>
                    </div>
                    <div className="d-flex align-items-start py-1">
                      <img
                        className=" float-left pl-1 pt-1 pricingcardimg"
                        src={listTick}
                      ></img>
                      <span className="pl-3  pricingcardpoints">
                        iOS and Android Web Applications Automated Testing
                      </span>
                    </div>
                  </div>
                  <div>
                    <button className="btn pricingcardbtn2">Contact Us</button>
                  </div>
                  <div className="pricingmore my-3">
                    <a>Show more details...</a>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-4 col-sm-6 col-12 card shadow col-12 mx-auto col-md-4 mt-5 mb-1 pricinglongcard">
              <div>
                <div className="card-body ">
                  <div style={{ minHeight: "80px " }}>
                    <img className="w-25 my-2" src={business}></img>
                  </div>
                  <h5 className="card-title pricingcardtitle">
                    On Premises - Enterprise
                  </h5>
                  <p className="card-text pricingcardtext">
                    For those who want to have the complete software inside
                    their network
                  </p>
                  <p className="pricingcardpricetext my-4">$ 95 / Month</p>
                  <div className="pricingcardbtn  w-100 mx-auto py-2 text-white">
                    Get Started
                  </div>
                  <div className="w-100 my-3 pricingcardpointdiv">
                    <div className="d-flex align-items-start py-1">
                      <img
                        className=" float-left pl-1 pt-1 pricingcardimg"
                        src={listTick}
                      ></img>
                      <span className="pl-3  pricingcardpoints">
                        Access to Web, Mobile, Web & Mobile, Web Services and
                        SFDC Automation
                      </span>
                    </div>
                    <div className="d-flex align-items-start py-1">
                      <img
                        className=" float-left pl-1 pt-1 pricingcardimg"
                        src={listTick}
                      ></img>
                      <span className="pl-3  pricingcardpoints">
                        Up to 99 Projects
                      </span>
                    </div>
                    <div className="d-flex align-items-start py-1">
                      <img
                        className=" float-left pl-1 pt-1 pricingcardimg"
                        src={listTick}
                      ></img>
                      <span className="pl-3  pricingcardpoints">
                        Up to 99 Users
                      </span>
                    </div>
                    <div className="d-flex align-items-start py-1">
                      <img
                        className=" float-left pl-1 pt-1 pricingcardimg"
                        src={listTick}
                      ></img>
                      <span className="pl-3  pricingcardpoints">
                        iOS and Android Web Applications Automated Testing
                      </span>
                    </div>
                    <div className="d-flex align-items-start py-1">
                      <img
                        className=" float-left pl-1 pt-1 pricingcardimg"
                        src={listTick}
                      ></img>
                      <span className="pl-3  pricingcardpoints">
                        Web Applications Automated Testing
                      </span>
                    </div>
                  </div>
                  <div>
                    <button className="btn pricingcardbtn2">Contact Us</button>
                  </div>
                  <div className="pricingmore my-3">
                    <a>Show more details...</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="mx-5">
          <table className="table table-borderless table-responsive-lg">
            <thead>
              <tr className="table_header">
                <th className="table_h border_left" scope="col"></th>
                <th className="table_h" scope="col">
                  Cloud Basic (Free)
                </th>
                <th className="table_h" scope="col">
                  Cloud Professional
                </th>
                <th className="table_h border_right" scope="col">
                  Enterprise - On Premises
                </th>
              </tr>
            </thead>
            <tbody>
              <tr className="border_left">
                <th scope="row" className="table_data pl-4 table_h border_left">
                  <p className="table_data_header pb-1">
                    Test Design & Development
                  </p>
                  Build unlimited test scripts
                </th>
                <td className="table_h">
                  <img className="table_icon pt-5" src={Cross} alt="" />
                </td>
                <td className="table_h">
                  <img className="table_icon pt-5" src={Tick} alt="" />
                </td>
                <td className="table_h border_right">
                  <img className="table_icon pt-5" src={Tick} alt="" />
                </td>
              </tr>
              <tr>
                <th scope="row" className="table_data pl-4 table_h border_left">
                  Create unlimited pages
                </th>
                <td className="table_h">
                  <img className="table_icon" src={Cross} alt="" />
                </td>
                <td className="table_h">
                  <img className="table_icon" src={Tick} alt="" />
                </td>
                <td className="table_h border_right">
                  <img className="table_icon" src={Tick} alt="" />
                </td>
              </tr>
              <tr>
                <th scope="row" className="table_data pl-4 table_h border_left">
                  Create unlimited elements/page
                </th>
                <td className="table_h">
                  <img className="table_icon" src={Cross} alt="" />
                </td>
                <td className="table_h">
                  <img className="table_icon" src={Tick} alt="" />
                </td>
                <td className="table_h border_right">
                  <img className="table_icon" src={Tick} alt="" />
                </td>
              </tr>
              <tr>
                <th scope="row" className="table_data pl-4 table_h border_left">
                  Create unlimited Program Elements
                </th>
                <td className="table_h">
                  <img className="table_icon" src={Cross} alt="" />
                </td>
                <td className="table_h">
                  <img className="table_icon" src={Tick} alt="" />
                </td>
                <td className="table_h border_right">
                  <img className="table_icon" src={Tick} alt="" />
                </td>
              </tr>
              <tr>
                <th scope="row" className="table_data pl-4 table_h border_left">
                  Create unlimited Test Step Groups
                </th>
                <td className="table_h">
                  <img className="table_icon" src={Cross} alt="" />
                </td>
                <td className="table_h">
                  <img className="table_icon" src={Tick} alt="" />
                </td>
                <td className="table_h border_right">
                  <img className="table_icon" src={Tick} alt="" />
                </td>
              </tr>
              <tr>
                <th scope="row" className="table_data pl-4 table_h border_left">
                  <p className="table_data_header pb-1">Test Execution</p>
                  Local Agent for test executions
                </th>
                <td className="table_h">
                  <img className="table_icon pt-5" src={Tick} alt="" />
                </td>
                <td className="table_h">
                  <img className="table_icon pt-5" src={Tick} alt="" />
                </td>
                <td className="table_h border_right">
                  <img className="table_icon pt-5" src={Tick} alt="" />
                </td>
              </tr>
              <tr>
                <th scope="row" className="table_data pl-4 table_h border_left">
                  Cross Browser Testing
                </th>
                <td className="table_h">
                  <img className="table_icon" src={Tick} alt="" />
                </td>
                <td className="table_h">
                  <img className="table_icon" src={Tick} alt="" />
                </td>
                <td className="table_h border_right">
                  <img className="table_icon" src={Tick} alt="" />
                </td>
              </tr>
              <tr>
                <th scope="row" className="table_data pl-4 table_h border_left">
                  Self-healing for Element ID
                </th>
                <td className="table_h">
                  <img className="table_icon" src={Tick} alt="" />
                </td>
                <td className="table_h">
                  <img className="table_icon" src={Tick} alt="" />
                </td>
                <td className="table_h border_right">
                  <img className="table_icon" src={Tick} alt="" />
                </td>
              </tr>
              <tr>
                <th scope="row" className="table_data pl-4 table_h border_left">
                  Cross Browser Testing on Cloud
                </th>
                <td className="table_h">
                  <img className="table_icon" src={Cross} alt="" />
                </td>
                <td className="table_h">
                  <img className="table_icon" src={Tick} alt="" />
                </td>
                <td className="table_h border_right">
                  <img className="table_icon" src={Tick} alt="" />
                </td>
              </tr>
              <tr>
                <th scope="row" className="table_data pl-4 table_h border_left">
                  Integration with collaboration tools
                </th>
                <td className="table_h">
                  <img className="table_icon" src={Cross} alt="" />
                </td>
                <td className="table_h">
                  <img className="table_icon" src={Tick} alt="" />
                </td>
                <td className="table_h border_right">
                  <img className="table_icon" src={Tick} alt="" />
                </td>
              </tr>
              <tr>
                <th scope="row" className="table_data pl-4 table_h border_left">
                  External Test Reports portal
                </th>
                <td className="table_h">
                  <img className="table_icon" src={Cross} alt="" />
                </td>
                <td className="table_h">
                  <img className="table_icon" src={Tick} alt="" />
                </td>
                <td className="table_h border_right">
                  <img className="table_icon" src={Tick} alt="" />
                </td>
              </tr>
              <tr>
                <th scope="row" className="table_data pl-4 table_h border_left">
                  <p className="table_data_header pb-1">
                    API and Backend Testing
                  </p>
                  Database and middleware testing
                </th>
                <td className="table_h">
                  <img className="table_icon pt-5" src={Tick} alt="" />
                </td>
                <td className="table_h">
                  <img className="table_icon pt-5" src={Tick} alt="" />
                </td>
                <td className="table_h border_right">
                  <img className="table_icon pt-5" src={Tick} alt="" />
                </td>
              </tr>
              <tr>
                <th scope="row" className="table_data pl-4 table_h border_left">
                  PDF, Email, SSH, Message Queue
                </th>
                <td className="table_h">
                  <img className="table_icon" src={Cross} alt="" />
                </td>
                <td className="table_h">
                  <img className="table_icon" src={Tick} alt="" />
                </td>
                <td className="table_h border_right">
                  <img className="table_icon" src={Tick} alt="" />
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      {/* Menu Tabs End Here */}

      <div>
        <h1 className="text-center py-4 question_header">Plan Comparison</h1>
        <nav>
          <div
            className="nav nav-tabs mx-5 d-flex justify-content-between"
            id="nav-tab"
            role="tablist"
          >
            <a
              className="nav-link active pricing_navtab"
              id="nav-home-tab"
              data-toggle="tab"
              href="#nav-home"
              role="tab"
              aria-controls="nav-home"
              aria-selected="true"
            >
              <div className="tab_card h-100">
                <div className="row no-gutters h-100">
                  <div className="col-md-4 my-auto">
                    <img className="pricingcardimage" src={avatar2} alt="..." />
                  </div>
                  <div className="col-md-8 my-auto">
                    <div className="card-body">
                      <h5 className="card-title tab_header">Cloud Basic</h5>
                      <p className="card-text tab_text">
                        For small teams trying out Live Demo for a year
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </a>
            <a
              className="nav-link pricing_navtab"
              id="nav-profile-tab"
              data-toggle="tab"
              href="#nav-profile"
              role="tab"
              aria-controls="nav-profile"
              aria-selected="false"
            >
              <div className="tab_card h-100">
                <div className="row no-gutters h-100">
                  <div className="col-md-4 my-auto">
                    <img
                      className="pricingcardimage"
                      src={groupavatar}
                      alt="..."
                    />
                  </div>
                  <div className="col-md-8 my-auto">
                    <div className="card-body">
                      <h5 className="card-title tab_header">
                        Cloud Professional
                      </h5>
                      <p className="card-text tab_text">
                        For small and medium sized businesses
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </a>
            <a
              className="nav-link pricing_navtab"
              id="nav-contact-tab"
              data-toggle="tab"
              href="#nav-contact"
              role="tab"
              aria-controls="nav-contact"
              aria-selected="false"
            >
              <div className="tab_card h-100">
                <div className="row no-gutters h-100">
                  <div className="col-md-4 my-auto">
                    <img
                      className="pricingcardimage"
                      src={business}
                      alt="..."
                    />
                  </div>
                  <div className="col-md-8 my-auto">
                    <div className="card-body">
                      <h5 className="card-title tab_header">
                        On Premises - Enterprise
                      </h5>
                      <span className="tab_text">
                        For those who want to have the complete software inside
                        their network
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </nav>
        <div className="tab-content" id="nav-tabContent">
          <div
            className="mx-5 py-5 tab-pane fade show active border_home"
            id="nav-home"
            role="tabpanel"
            aria-labelledby="nav-home-tab"
          >
            <div className="container tab-pane active">
              <div className="row d-flex justify-content-between mx-4">
                <div className="col-md-3">
                  <h1 className="tab_content_title">Book your Demo!</h1>
                  <p className="tab_content_text">
                    See how TextOptimize provides simple solutions for your
                    testing needs.
                  </p>
                  <div className="col-md-12">
                    <button
                      className="btn reqdemobtn"
                      style={{ borderRadius: "8px" }}
                    >
                      <Link
                        to="/requestdemo"
                        style={{ textDecoration: "none" }}
                      >
                        <p
                          className="m-0 text-white"
                          style={{ fontSize: "10px" }}
                        >
                          REQUEST A DEMO
                        </p>
                      </Link>
                    </button>
                  </div>
                  <div>
                    <div className="d-flex mt-3">
                      <span>
                        <img style={{ maxWidth: "32px" }} src={pin} alt="" />
                      </span>
                      <p className="tab_Content_p">
                        Access to Web Services, Web, Mobile and Web & Mobile
                        Automation
                      </p>
                    </div>
                    <div className="d-flex mt-3">
                      <span>
                        <img style={{ maxWidth: "32px" }} src={pin} alt="" />
                      </span>
                      <p className="tab_Content_p">
                        Access to all features of the product
                      </p>
                    </div>
                    <div className="d-flex mt-3">
                      <span>
                        <img style={{ maxWidth: "32px" }} src={pin} alt="" />
                      </span>
                      <p className="tab_Content_p">
                        Web, Android and iOS Automated Testing
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-md-3">
                  <h1 className="tab_content_title">
                    Experience all plans for FREE!
                  </h1>
                  <p className="tab_content_text">
                    Welcome to the world of Live Demo
                  </p>
                  <div className="col-md-12">
                    <button className="btn getStartbtn">
                      <p
                        className="m-0 text-white"
                        style={{ fontSize: "10px" }}
                      >
                        Get Started
                      </p>
                    </button>
                  </div>
                  <div>
                    <div className="w-100 my-3  justify-content-center">
                      <div className="d-flex align-items-start py-1">
                        <img
                          className=" float-left pl-1 pt-1 pricingcardimg invisible"
                          src={listTick}
                        ></img>
                        <p className="pl-3   tab_Content_project">
                          Project Types
                        </p>
                      </div>
                      <div className="d-flex align-items-start py-1">
                        <img
                          className=" float-left pl-1 pt-1 pricingcardimg"
                          src={listTick}
                        ></img>
                        <span className="pl-3  pricingcardpoints">
                          Web Service Projects
                        </span>
                      </div>
                      <div className="d-flex align-items-start py-1">
                        <img
                          className=" float-left pl-1 pt-1 pricingcardimg"
                          src={listTick}
                        ></img>
                        <span className="pl-3  pricingcardpoints">
                          Web Projects
                        </span>
                      </div>
                      <div className="d-flex align-items-start py-1">
                        <img
                          className=" float-left pl-1 pt-1 pricingcardimg"
                          src={listTick}
                        ></img>
                        <span className="pl-3  pricingcardpoints">
                          Mobile Projects
                        </span>
                      </div>
                      <div className="d-flex align-items-start py-1">
                        <img
                          className=" float-left pl-1 pt-1 pricingcardimg"
                          src={listTick}
                        ></img>
                        <span className="pl-3  pricingcardpoints">
                          Web & Mobile Projects
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-3">
                  <div>
                    <div className="w-100 my-3  justify-content-center">
                      <div className="d-flex align-items-start py-1">
                        <img
                          className=" float-left pl-1 pt-1 pricingcardimg invisible "
                          src={listTick}
                        ></img>
                        <h1 className="pl-3 tab_content_title">
                          You can Automate
                        </h1>
                      </div>
                      <div className="d-flex align-items-start py-1">
                        <img
                          className=" float-left pl-1 pt-1 pricingcardimg"
                          src={listTick}
                        ></img>
                        <span className="pl-3  pricingcardpoints">
                          Upto 100 Test Scripts
                        </span>
                      </div>
                      <div className="d-flex align-items-start py-1">
                        <img
                          className=" float-left pl-1 pt-1 pricingcardimg"
                          src={listTick}
                        ></img>
                        <span className="pl-3  pricingcardpoints">
                          Upto 50 Elements
                        </span>
                      </div>
                      <div className="d-flex align-items-start py-1">
                        <img
                          className=" float-left pl-1 pt-1 pricingcardimg"
                          src={listTick}
                        ></img>
                        <span className="pl-3  pricingcardpoints">
                          Upto 50 Program Elements
                        </span>
                      </div>
                      <div className="d-flex align-items-start py-1">
                        <img
                          className=" float-left pl-1 pt-1 pricingcardimg"
                          src={listTick}
                        ></img>
                        <span className="pl-3  pricingcardpoints">
                          Upto 25 Step Groups
                        </span>
                      </div>
                      <div className="d-flex align-items-start py-1">
                        <img
                          className=" float-left pl-1 pt-1 pricingcardimg"
                          src={listTick}
                        ></img>
                        <span className="pl-3  pricingcardpoints">
                          Upto 5 Parallel runs
                        </span>
                      </div>
                      <div className="d-flex align-items-start py-1">
                        <img
                          className=" float-left pl-1 pt-1 pricingcardimg"
                          src={listTick}
                        ></img>
                        <span className="pl-3  pricingcardpoints">
                          Upto 3 Users
                        </span>
                      </div>
                      <div className="d-flex align-items-start py-1">
                        <img
                          className=" float-left pl-1 pt-1 pricingcardimg"
                          src={listTick}
                        ></img>
                        <span className="pl-3  pricingcardpoints">
                          Upto 10GB Memory
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div
            className="tab-pane fade mx-5 py-5 border_menu1"
            id="nav-profile"
            role="tabpanel"
            aria-labelledby="nav-profile-tab"
          >
            <div className="container tab-pane active">
              <div className="row d-flex justify-content-between mx-4">
                <div className="col-md-2 mx-2">
                  <p className="text-left dropdown_text">Select Plan</p>
                  <div className="form-group">
                    <select
                      name=""
                      id=""
                      className="form-control dropdown_option"
                    >
                      <option value="" selected>
                        ALL
                      </option>
                    </select>
                  </div>
                </div>
                <div className="col-md-2 mx-2">
                  <p className="text-left dropdown_text">Billing Cycle</p>
                  <div className="form-group">
                    <select
                      name=""
                      id=""
                      className="form-control dropdown_option"
                    >
                      <option value="" selected>
                        Monthly
                      </option>
                    </select>
                  </div>
                </div>
                <div className="col-md-2 mx-2">
                  <p className="text-left dropdown_text">No: of Users</p>
                  <div className="form-group">
                    <select
                      name=""
                      id=""
                      className="form-control dropdown_option"
                    >
                      <option value="" selected>
                        1
                      </option>
                    </select>
                    <p className="text-left dropdown_text">$75/ User</p>
                  </div>
                </div>
                <div className="col-md-2 mx-2">
                  <p className="dropdown_p text-right">$ 95 / Month</p>
                  <p className="dropdown_p_text">
                    Welcome to the world of Live Demo
                  </p>
                  <button className="btn ml-4 buy_button">Buy Now</button>
                </div>
              </div>
              <div className="mx-5 mt-4">
                <table className="table table-sm table-borderless table-responsive-lg">
                  <thead>
                    <tr className="table_header">
                      <th className="table_h border_left" scope="col"></th>
                      <th className="table_heading" scope="col">
                        C-Web & Mobile Professional
                      </th>
                      <th className="table_heading" scope="col">
                        C-Web Professional
                      </th>
                      <th className="table_heading border_right" scope="col">
                        C-Mobile Professional
                      </th>
                      <th className="table_heading border_right" scope="col">
                        C-WebServices Professional
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <p className="table_data_header py-3 pl-4">Project Types</p>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h text-info border_left"
                      >
                        Web & Mobile Projects
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                    </tr>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h text-info border_left"
                      >
                        Web Projects
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                    </tr>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h text-info border_left"
                      >
                        Mobile Projects
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                    </tr>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h text-info border_left"
                      >
                        Web Services Projects
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                    </tr>
                    <p
                      className="table_data_header py-3 pl-4"
                      style={{ whiteSpace: "nowrap" }}
                    >
                      Test Design & Development
                    </p>
                    <tr className="border_left">
                      <th
                        scope="row"
                        className="table_data pl-4 table_h border_left"
                      >
                        Build unlimited test scripts
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                    </tr>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h border_left"
                      >
                        Create unlimited pages
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                    </tr>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h border_left"
                      >
                        Create unlimited elements/page
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                    </tr>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h border_left"
                      >
                        Create unlimited Program Elements
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                    </tr>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h border_left"
                      >
                        Create unlimited Test Step Groups
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                    </tr>
                    <p className="table_data_header py-3 pl-4">
                      Test Execution
                    </p>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h border_left"
                      >
                        Local Agent for test executions
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                    </tr>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h border_left"
                      >
                        Cross Browser Testing
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                    </tr>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h border_left"
                      >
                        Self-healing for Element ID
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                    </tr>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h border_left"
                      >
                        Cross Browser Testing on Cloud
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                    </tr>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h border_left"
                      >
                        Integration with collaboration tools
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                    </tr>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h border_left"
                      >
                        External Test Reports portal
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                    </tr>
                    <p className="table_data_header py-3 pl-4">
                      API and Backend Testing
                    </p>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h border_left"
                      >
                        Database and middleware testing
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                    </tr>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h border_left"
                      >
                        PDF, Email, SSH, Message Queue
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div
            className="tab-pane fade mx-5 py-5 border_menu2"
            id="nav-contact"
            role="tabpanel"
            aria-labelledby="nav-contact-tab"
          >
            <div className="container tab-pane active">
              <div className="row d-flex justify-content-between mx-4">
                <div className="col-md-2 mx-2">
                  <p className="text-left dropdown_text">Select Plan</p>
                  <div className="form-group">
                    <select
                      name=""
                      id=""
                      className="form-control dropdown_option"
                    >
                      <option value="" selected>
                        ALL
                      </option>
                    </select>
                  </div>
                </div>
                <div className="col-md-2 mx-2">
                  <p className="text-left dropdown_text">Billing Cycle</p>
                  <div className="form-group">
                    <select
                      name=""
                      id=""
                      className="form-control dropdown_option"
                    >
                      <option value="" selected>
                        Monthly
                      </option>
                    </select>
                  </div>
                </div>
                <div className="col-md-2 mx-2">
                  <p className="text-left dropdown_text">No: of Users</p>
                  <div className="form-group">
                    <select
                      name=""
                      id=""
                      className="form-control dropdown_option"
                    >
                      <option value="" selected>
                        1
                      </option>
                    </select>
                    <p className="text-left dropdown_text">$75/ User</p>
                  </div>
                </div>
                <div className="col-md-2 mx-2">
                  <p className="dropdown_p text-right">$ 95 / Month</p>
                  <p className="dropdown_p_text">
                    Welcome to the world of Live Demo
                  </p>
                  <button className="btn ml-4 buy_button">Buy Now</button>
                </div>
              </div>
              <div className="mx-5 mt-4">
                <table className="table table-sm table-borderless table-responsive-lg">
                  <thead>
                    <tr className="table_header">
                      <th className="table_h border_left" scope="col"></th>
                      <th className="table_heading" scope="col">
                        C-Web & Mobile Professional
                      </th>
                      <th className="table_heading" scope="col">
                        C-Web Professional
                      </th>
                      <th className="table_heading border_right" scope="col">
                        C-Mobile Professional
                      </th>
                      <th className="table_heading border_right" scope="col">
                        C-WebServices Professional
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <p className="table_data_header py-3 pl-4">Project Types</p>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h text-info border_left"
                      >
                        Web & Mobile Projects
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                    </tr>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h text-info border_left"
                      >
                        Web Projects
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                    </tr>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h text-info border_left"
                      >
                        Mobile Projects
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                    </tr>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h text-info border_left"
                      >
                        Web Services Projects
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                    </tr>
                    <p
                      className="table_data_header py-3 pl-4"
                      style={{ whiteSpace: "nowrap" }}
                    >
                      Test Design & Development
                    </p>
                    <tr className="border_left">
                      <th
                        scope="row"
                        className="table_data pl-4 table_h border_left"
                      >
                        Build unlimited test scripts
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                    </tr>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h border_left"
                      >
                        Create unlimited pages
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                    </tr>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h border_left"
                      >
                        Create unlimited elements/page
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                    </tr>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h border_left"
                      >
                        Create unlimited Program Elements
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                    </tr>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h border_left"
                      >
                        Create unlimited Test Step Groups
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                    </tr>
                    <p className="table_data_header py-3 pl-4">
                      Test Execution
                    </p>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h border_left"
                      >
                        Local Agent for test executions
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                    </tr>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h border_left"
                      >
                        Cross Browser Testing
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                    </tr>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h border_left"
                      >
                        Self-healing for Element ID
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                    </tr>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h border_left"
                      >
                        Cross Browser Testing on Cloud
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                    </tr>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h border_left"
                      >
                        Integration with collaboration tools
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                    </tr>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h border_left"
                      >
                        External Test Reports portal
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                    </tr>
                    <p className="table_data_header py-3 pl-4">
                      API and Backend Testing
                    </p>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h border_left"
                      >
                        Database and middleware testing
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                    </tr>
                    <tr>
                      <th
                        scope="row"
                        className="table_data pl-4 table_h border_left"
                      >
                        PDF, Email, SSH, Message Queue
                      </th>
                      <td className="table_h">
                        <img className="table_icon" src={Cross} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                      <td className="table_h border_right">
                        <img className="table_icon" src={Tick} alt="" />
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* Questions Card Section Starts Here */}
      <div className="questionbg py-4">
        <h1 className="text-center question_header py-4">
          Frequently Asked Questions
        </h1>
        <div
          className="card shadow mx-auto"
          style={{ width: "85%", borderRadius: "8px" }}
        >
          <div>
            <ul className="question_text mx-auto">
              <li className="my-3">
                What is the difference between Cloud and On-Premise offering ?
              </li>
              <li className="my-3">What does a Cloud Basic include ?</li>
              <li className="my-3">What does a Cloud Professional include ?</li>
              <li className="my-3">
                What does a On-Primise Enterprise include ?
              </li>
              <li className="my-3">
                What is included in the cloud hosting option ?
              </li>
              <li className="my-3">
                Is the license assigned as a Named User or is it based on
                Concurrent usage model ?
              </li>
              <li className="my-3">
                How secured is the Cloud hosting for my data ?
              </li>
              <li className="my-3">
                Is there an option to host on private cloud of my organization
                choice ?
              </li>
              <li className="my-3">
                What is the pricing for customer support ?
              </li>
              <li className="my-3">Can I cancel my subscription ?</li>
            </ul>
          </div>
          <div className="question_footer py-3">
            <h1>
              Looking for something else ? <span> Contact us</span>
            </h1>
          </div>
        </div>
      </div>
      {/* Questions Card Section Ends Here */}
      <FooterComponent
        background={"linear-gradient(to Right, #074784, #1aa2c0)"}
      />
    </div>
  );
};

export default Pricing;
