import "./Mobile.css";
import React from "react";
import mobiletesting from "../../assets/mobiletesting.png";
import mbtestheading from "../../assets/mbtestheading.png";
import mblogo from "../../assets/mblogo.png";
import mobileapple from "../../assets/mobileapple.png";
import mobileandroid from "../../assets/mobileandroid.png";
import mbs3icon1 from "../../assets/mbs3icon1.png";
import mbs3icon2 from "../../assets/mbs3icon2.png";
import mbs2ic1 from "../../assets/mbs2ic1.png";
import mbs2ic2 from "../../assets/mbs2ic2.png";
import mbs2ic3 from "../../assets/mbs2ic3.png";
import mbs4ic1 from "../../assets/mbs4ic1.png";
import mobilebg from "../../assets/mobilebg.png";
import custombg from "../../assets/custombg.png";
import footerbg from "../../assets/footerbg.png";
import play from "../../assets/Footer/play.png";
import HeaderComponent from "../HeaderComponent/HeaderComponent";
import FooterComponent from "../FooterComponent/FooterComponent";

import api_div6_img120 from "../../assets/api_div6_img120.png";
import api_div6_img220 from "../../assets/api_div6_img220.png";
import api_div8_img320 from "../../assets/api_div8_img320.png";

const Mobile = ({ activePage }) => {
  return (
    <div>
      <section
        className="webby align-items-center"
        style={{ background: `url(${mobilebg})` }}
      >
        <div>
          <HeaderComponent activePage={activePage} />
        </div>
        <div className="container" style={{ marginTop: "15%" }}>
          <div className="row firstrow0 mr-4 mobile_seconddiv">
            <div
              className="col-lg-6 mobile_thirddiv"
              style={{ paddingRight: "3%", paddingLeft: "3%" }}
            >
              <div className="col-lg-12 mb-5">
                <img src={mblogo} className="firstdivlogo1" alt="" />
              </div>
              <div className="col-lg-12  ">
                <img src={mbtestheading} className="firstdivweb1" alt="" />
              </div>
              <div className="col-lg-12  ">
                <h5 className="text-left firstdivttl mb-4">
                  Develop your web test scripts with ease
                </h5>
                <p className="second">
                  TestOptimize uses AI to create stable and reliable automated
                  tests faster than ever and to speed-up the executions and
                  maintenance of your automated tests. No coding skills
                  required.
                </p>
              </div>
            </div>
            <div className="col-lg-6 hero-img mt-5">
              <img src={mobiletesting} className="img-fluid" alt="" />
            </div>
          </div>
        </div>
      </section>

      <div className="container mx-auto mb-30 smalldiv col-lg-8">
        <div className="pt-3">Simple solution to Automate</div>
      </div>

      <section
        id="section2"
        className="section2 d-flex align-items-center"
        style={{ marginBottom: "-5%" }}
      >
        <div className="container">
          <div className="row ">
            <div className="col-lg-4 col-md-4 col-sm-12 mb-4 ">
              <div className="col-lg-12 col-md-4 col-sm-12 maddy">
                <img src={mbs2ic3} className="mbs2Img" alt="" />
              </div>
              <div className="col-lg-12 ">
                <h5 className=" datatitle1 mb-4">Native Apps</h5>
                <p className=" subdata1 ">
                  With AI deiven now yoy can avoid writing repeated steps which
                  come across in all your test cases
                </p>
              </div>
            </div>

            <div className="col-lg-4 col-md-4 col-sm-12 mb-4">
              <div className="col-lg-12 col-md-4 col-sm-12 maddy">
                <img src={mbs2ic2} className="mbs2Img2" alt="" />
              </div>
              <div className="col-lg-12  ">
                <h5 className=" datatitle1 mb-4">Web Apps</h5>
                <p className=" subdata1 ml-5">
                  Define your own NLP's which demands your test scenario
                </p>
              </div>
            </div>
            <div className="col-lg-4 col-md-4 col-sm-12 mb-4">
              <div className="col-lg-12 col-md-4 col-sm-12 maddy ">
                <img src={mbs2ic1} className="mbs2Img" alt="" />
              </div>

              <div className="col-lg-12 ">
                <h5 className=" datatitle1 mb-4">Hybrid Apps</h5>
                <p className=" subdata1 ">
                  Define your own NLP's which demands your test scenario
                </p>
              </div>
            </div>
          </div>

          <div className="row mx-auto col-lg-8 col-md-12 smalldiv">
            <div className="container mt-3">Supports Multiple Platform</div>
          </div>

          <div className="container row col-lg-8 mx-sm-3 mx-md-5 mx-lg-auto my-20">
            <div className="col-lg-6 col-md-5  mt-5">
              <img src={mobileapple} width="200px" height="250px" alt="" />
            </div>
            <div className="col-lg-6 col-md-4  mt-5">
              <img src={mobileandroid} width="300px" height="250px" alt="" />
            </div>
          </div>
        </div>
      </section>

      <section
        id="section3"
        className="section3 d-flex align-items-center mt-5 pb-4"
      >
        <div className="row col-lg-12">
          <div className="col-lg-6 mt-5">
            <h5 className="sectionTitle ">Run your mobile test scripts</h5>
            <p className=" sectionSubTitle mt-3">
              TestOptimize supports executing your mobile test scripts on
              multiple cloud platforms
            </p>
          </div>
          <div className="col-lg-6">
            <div className=" mx-auto">
              <img src={mbs3icon1} width="150px" height="150px" alt="" />
              <img src={mbs3icon2} width="150px" height="150px" alt="" />
            </div>
          </div>
        </div>
      </section>

      <div className="container mx-auto smalldiv col-lg-8">
        <div className="pt-3">Supports Various Virtual Devices</div>
      </div>

      <section
        id="section4"
        className="section4 d-flex align-items-center mb-5 "
      >
        <div className="position-absolute borderRight"></div>
        <div className="row col-lg-4 mx-auto ">
          <div className="col-lg-5 ">
            <img src={mbs4ic1} height="300px" alt="" />
          </div>
          <div className="col-lg-7 my-auto">
            <h5 className=" datatitle mb-2 ">Simulators</h5>
            <p className=" subdata ">
              Run your mobile test scripts on Simulators
            </p>
            <div className="row subdata align-center col-lg-10 mx-auto">
              <div className="col-lg-2 col-md-2 mx-auto m-0 p-0 ">
                <img src={play} className="playButtonImage" alt="" />
              </div>
              <div className=" col-lg-9 col-md-12 m-0 p-0 my-auto">
                <span className="playButtonText">Watch the Video</span>
              </div>
            </div>
          </div>
        </div>

        <div className="row col-lg-4 mx-auto p-0">
          <div className="col-lg-7 my-auto">
            <h5 className=" datatitle mb-2">Emulators</h5>
            <p className=" subdata ">
              Run your mobile test scripts on Emulators
            </p>
            <div className="row subdata align-center col-lg-10 mx-auto">
              <div className="col-lg-2 col-md-2 mx-auto m-0 p-0 ">
                <img src={play} className="playButtonImage" alt="" />
              </div>
              <div className=" col-lg-9 col-md-12 m-0 p-0 my-auto">
                <span className="playButtonText">Watch the Video</span>
              </div>
            </div>
          </div>
          <div className="col-lg-5">
            <img src={mbs4ic1} height="300px" alt="" />
          </div>
        </div>
      </section>

      <section
        id="herobrand"
        className="section1 align-items-center"
        style={{ background: `url(${custombg})` }}
      >
        <div
          className="container-fluid custombg "
          style={{ marginBottom: "-5%" }}
        >
          <div className="row col-md-10 mx-auto">
            <div className="col-lg-6 col-md-12 col-sm-12 ">
              <div className="col-lg-12">
                <img src={api_div6_img120} className="custy" alt="" />
              </div>
              <div className="col-lg-12 ">
                <h5 className=" mbs5TitleColor mb-2">Steps Reusability</h5>
                <p className=" mbs5SubTitleColor ">
                  With AI deiven now yoy can avoid writing repeated steps which
                  come across in all your test cases
                </p>
              </div>
            </div>
            <div className="col-lg-6 col-md-12 col-sm-12 mb-4">
              <div className="col-lg-12 sixthdivcol2">
                <img src={api_div6_img220} className="custy" alt="" />
              </div>
              <div className="col-lg-12 ">
                <h5 className=" mbs5TitleColor ">Custom Functions</h5>
                <p className=" mbs5SubTitleColor ">
                  Define your own NLP's which demands your test scenario
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>

      <div className="sevendivbg">
        <div className="container" style={{ marginBottom: "-1%" }}>
          <div className="row mt-3 ">
            <div
              className="col-lg-6 col-md-12 col-sm-12 "
              style={{ marginBottom: "-10%" }}
            >
              <img src={api_div8_img320} className="divineimg" alt="" />
            </div>
            <div className="col-lg-6 col-md-12 lg-4 col-sm-12 ">
              <ul className="ulList100">
                <li className="listdivtit">
                  Execute your automated mobile app tests faster than ever
                </li>
                <li>
                  Create your mobile automated test cases quickly with natural
                  language-based test step creation that enables you to
                  formulate test steps using simple English statements.
                </li>
                <li style={{ marginTop: "3%" }}>
                  Centralized Object Repository Test Data Management, and
                  Reusable Steps Groups will help you speed up automating tests.
                </li>
              </ul>
            </div>
          </div>

          <div className="row ">
            <div className="col-lg-6 col-md-12 lg-4 col-sm-12 ">
              <ul className="ulList100 ">
                <li className="listdivtit">
                  Execute your automated mobile app tests parallely to reduce
                  time
                </li>
                <li>
                  Your mobile test cases can be executed concurrently on
                  multiple devices thereby reducing the execution time in the
                  order of the number of test enviornments configured for
                  execution.
                </li>
                <li style={{ marginTop: "3%" }}>
                  Faster execution helps find bugs at the earliest so that you
                  don't have to wait longer to start user acceptance testing
                  after the build(RC) is available.
                </li>
              </ul>
            </div>
            <div className="col-lg-6 col-md-12 col-sm-12 mb-5">
              <img src={api_div8_img320} className="divimg" alt="" />
            </div>
          </div>
        </div>
      </div>
      <div>
        <FooterComponent background={footerbg} image={true} />
      </div>
    </div>
  );
};

export default Mobile;
