import "./Database.css";
import React from "react";
import HeaderComponent from "../HeaderComponent/HeaderComponent";
import FooterComponent from "../FooterComponent/FooterComponent";
import backgroungimg from "../../assets/backgroungimg.png";
import firstdivweb from "../../assets/firstdivweb.png";
import Layer7 from "../../assets/database/Layer 7.png";
import Layer8 from "../../assets/database/Layer8.png";
import pin from "../../assets/database/pin.png";
import Layer11 from "../../assets/database/Layer11.png";
import awsdynamodb from "../../assets/database/awsdynamodb.png";
import Layer10 from "../../assets/database/Layer10.png";
import kiss from "../../assets/database/kiss.png";
import alternative from "../../assets/database/alternative.png";
import api_div6_img120 from "../../assets/api_div6_img120.png";
import api_div6_img220 from "../../assets/api_div6_img220.png";
import api_div8_img320 from "../../assets/api_div8_img320.png";
import Layer28 from "../../assets/database/Layer28.png";
import custombg from "../../assets/custombg.png";
import Layer5 from "../../assets/database/Layer5.png";
import coverter from "../../assets/database/coverter.png";
import convey from "../../assets/database/convey.png";
import Layer16 from "../../assets/database/Layer16.png";
import Layer17 from "../../assets/database/Layer17.png";
import effil from "../../assets/database/effil.png";
import Group21 from "../../assets/database/Group21.svg";

function Database() {
    return (
      <div style={{overflowX:"hidden"}}>
        <section
          id="data_Hero"
          className="data_Hero align-items-center"
          style={{ background: `url(${Layer5})`}}
        >
          <div>
            <HeaderComponent />
          </div>
          <div className="container-fluid">
            <div
              className="row justify-content-lg-center data_firstrow data_thirddiv "
              style={{ marginTop: "5%" }}
            >
              <div
                className="col-lg-6 data_seconddiv"
                style={{ paddingRight: "8%" , paddingLeft : "8%" }}
              >
                <div className="col-lg-12 mb-4">
                  <img src={Layer7} className="data_firstdivlogo " alt="" />
                </div>
                <br />
                <div className="col-lg-12 mr-10 datatitile1223 ">
                  <p >TestOptimize-Database</p>
                </div>
                <div className="col-lg-12">
                  <h5 className="web_text-left data_firstdivtitle mb-4">
                  Develop Database test scripts with ease
                  </h5>
                  <p className="web_text-left data_firstdivcontent">
                  TestOptimize uses AI to create stable and
                   reliable automated tests faster than ever
                    and to speed-up the executions and 
                    maintenance of your automated tests.
                     No coding skills required.
                  </p>
                </div>
              </div>
              <div className="col-lg-6 data_hero-img">
                <div className="container data_img-fluid">
                  <img
                    src={alternative}
                    style={{
                      maxWidth: "100%",
                      marginTop: "0%",
                      marginBottom: "0%",
                    }}
                  />                  
                </div>
              </div>
            </div>
          </div>
        </section>

        <div className="container mx-auto mb-30 smalldiv col-lg-8">
        <div className="pt-3">Supports widely used Databases</div>
      </div>


<div className="container-fluid api_seconddivbg">
        <div className=" mbs2Imgdata row mt-5 ">
          <div className="col col-lg-4  ">
            <img
              src={kiss}
              style={{ width: "40%", marginTop: "15%" }}
              alt=""
            />
          </div>
          <div className="col col-lg-4">
            <img
              src={Layer8}
              style={{ width: "40%", marginTop: "25%" }}
              alt=""
            />
          </div>
          <div className="col col-lg-4">
            <img
              src={pin}
              style={{ width: "40%", marginTop: "25%" }}
              alt=""
            />
          </div>
        </div>

        <div className="row mt-5 mb-5">
          <div className="col col-lg-4">
            <img
              src={Layer10}
              style={{ width: "40%", marginTop: "5%" }}
              alt=""
            />
          </div>
          <div className="col col-lg-4">
            <img src={awsdynamodb} style={{ width: "40%" }} alt="" />
          </div>
          <div className="col col-lg-4">
            <img src={Layer11} style={{ width: "40%" }} alt="" />
          </div>
        </div>
        </div>

      <div className="container mx-auto mb-30 smalldiv col-lg-8">
        <div className="pt-3">Inbuilt framework for all your algorithms </div>
      </div>
      

{/* adding image with points */}

<div className="data_divthird">
        <div className="container">
          <div className="row">
            <div className="col-lg-6 col-md-12 mt-lg-4 col-sm-12">
              <ul className="data_ulListtwo ">
                <li className="data_liList ">
                Embedded Data driven framework
                </li>
                <li className="data_liList">
                Request payload created with parameterized data
                </li>
                <li className="data_liList">
                Parameterize assertions with dynamic data
                </li>
                <li className="data_liList">
                Automatically propagate assertions across repeated sets of response
                </li>
                <li className="data_liList">
                Easily call server APIs from your UI test for validation or data inquiry
                </li>
              </ul>
            </div>
            <div className="col-lg-6 col-md-12 col-sm-12">
              <img src={coverter} className="divsecondimgdata" alt="" />
            </div>
          </div>
        </div>
      </div>



<section
    id="herobrand"
    className="section1 align-items-center"
    style={{ background: `url(${Layer5 })` }}
  >
<div className="container-fluid position-relative">
        <div
          className="data_bar_hide position-absolute"
          style={{
            borderRight: "1px solid #ffffff",
            height: "25%",
            bottom: "0%",
            top: "53%",
            width: "34%",
          }}
        ></div>
        <div
          className="data_bar_hide position-absolute"
          style={{
            borderRight: "1px solid #ffffff",
            height: "25%",
            bottom: "0%",
            top: "53%",
            width: "64%",
          }}
        ></div>
        <div className="row ">
          <div className="col-lg-4 col-md-12 col-sm-12 ">
            <div className="col-lg-12 data_Fourthdivcol1">
              <img src={Layer16} className="data_custom1" alt="" />
            </div>
            <div className="col-lg-12 ">
              <h5 className="api_text-left mbs5TitleColor1 mb-2">
              SQL & NoSQL Databases
              </h5>
              <p className="api_text-left mbs5SubTitleColor1 ">
              JSON RAML Swagger / Open API WADL SOA / Web services/ XML WSDL / XML
               Schema SOAP PoX (Plain XML) /GZIP WS-*Standards
              </p>
            </div>
          </div>
          <div className="col-lg-4 col-md-12 col-sm-12">
            <div className="col-lg-12 ">
              <img src={effil} className="data_custom1" alt="" />
            </div>
            <div className="col-lg-12 ">
              <h5 className="api_text-left mbs5TitleColor1 mb-2">
              On Premises
              </h5>
              <p className="api_text-left mbs5SubTitleColor1 ">
              Kafka, RabbitMQ, MQTT, AMQP Protobuf WebSockets
              </p>
            </div>
          </div>
          <div className="col-lg-4 col-md-12 col-sm-12">
            <div className="col-lg-12 mb-4 ">
              <img src={Layer17} className="data_custom1" alt="" />
            </div>
            <div className="col-lg-12 api_Fouthdivcol2">
              <h5 className="api_text-left mbs5TitleColor1 mb-3 ">
              Cloud DB Testing
              </h5>
              <p className="api_text-left mbs5SubTitleColor1 mb-5 ">
              Open API WADL 50A/ Web services XML WSDL XML Schema
                 SOAP poX(Plain XML) GZIP WS_*Standards
              </p>
            </div>
          </div>
        </div>
      </div>
      
        </section>


{/* scriptless database */}

<div className="web_testdiv">
        <div className="container ">
          <div className="row ">
            <div className="col-lg-6 col-md-12 col-sm-12 ">
              <img src={convey} className="data_computerimg" alt="" />
            </div>
           
            <div className="col-lg-4 col-md-12 mt-lg-4 col-sm-12 pb-5">
                
            <div className="listdivtitdata1">
                <p>Scriptless Database Test Automation,
                     Seamlessly integrated with UI</p>
            </div>
              <ul className="data_ulListtwo  ">
             
                <li className="data_liList  ">
                Low Code API testing on cloud
                </li>
                <li className="data_liList">
                API & UI Testing in single Test Script
                </li>
                <li className="data_liList">
                API Test Case Management, Test Planning, Execution and tracking governance
                </li>
                <li className="data_liList">
                CI Driven regression suite executions
                </li>
                <li className="data_liList">
                Dynamic Environment management
                </li>
                <li className="data_liList">
                Directly correlate Business process and corresponding API for complete coverage
                </li>
              </ul>
            </div>
          </div>
          </div>
          </div>


{/* footer section */}
<section
    id="herobrand"
    className="section1 align-items-center"
    style={{ background: `url(${Layer5 })` }}
  >
      <div className="container-fluid " style={{ marginBottom: "-6%" }}>
        <div className="row col-md-10 mx-auto">
          <div className="col-lg-6 col-md-12 col-sm-12 ">
            <div className="col-lg-12">
              <img src={api_div6_img120} className="custy" alt="" />
            </div>
            <div className="col-lg-12 ">
              <h5 className=" mbs5TitleColor mb-2">
              Database testing using NLPs
              </h5>
              <p className=" mbs5SubTitleColor ">
              With AI driven now you can avoid writing repeated 
              steps which come across in all your test cases
              </p>
            </div>
          </div>
          <div className="col-lg-6 col-md-12 col-sm-12 mb-4">
            <div className="col-lg-12 sixthdivcol2">
              <img src={api_div6_img220} className="custy" alt="" />
            </div>
            <div className="col-lg-12 ">
              <h5 className=" mbs5TitleColor ">Custom Asserts</h5>
              <p className=" mbs5SubTitleColor ">
               Define your own NLp’s which demands your test scenario
              </p>
            </div>
          </div>
        </div>
      </div>
</section>



      <div className="sevendivbg">
        <div className="container ">
          <div className="row" style={{ marginTop: "-5%" }}>
            <div
              className="col-lg-6 col-md-12 col-sm-12 "
              style={{ marginBottom: "-5%" }}
            >
              <img src={api_div8_img320} className="screen123" alt="" />
            </div>
            <div className="col-lg-6 col-md-12 lg-4 col-sm-12 ">
              <ul className="ulList100">
                
                <li className="listdivtitdata" style={{ marginTop: "25%" }}>
                Execute your Database Test Suite faster than ever
                </li>
                <li>
                Create your API automated test cases quickly with natural 
                language-based test step creation that enables you to 
                formulate test steps using simple English statements.
                </li>
                <li style={{ marginTop: "3%" }}>
                Test Data Management, and Reusable Step Groups
                 will help you speed up automating tests.
                </li>
              </ul>
            </div>
          </div>

          <div className="row" >
            <div className="col-lg-6 col-md-12 lg-4 col-sm-12" >
              <ul className="ulList100 " >
                <li className="listdivtitdata">
                Simple Solution to your Database regression suite
                </li>
                <li>
                Create your API automated test cases quickly with natural 
                language-based test step creation that enables you to 
                formulate test steps using simple English statements.
                </li>
                <li style={{ marginTop: "3%" }}>
                Test Data Management, and Reusable Step Groups will
                 help you speed up automating tests.
                </li>
              </ul>
            </div>
            <div className="col-lg-6 col-md-12 col-sm-12 mb-5">
              <img src={api_div8_img320} className="screen321" alt="" />
            </div>
          </div>
        </div>
      </div>
      <div>
        <FooterComponent background={Layer28} image={true} />
      </div>
        </div>
    )}

    export default Database;