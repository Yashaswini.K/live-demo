import React from "react";
import "./Modal1.css";
export default function Modal1({ modal, modalClose, modalValue }) {
  return (
    <div
      style={{ display: modal && modal ? "block" : "none" }}
      className="homemodal1 "
    >
      <div className="h-100 d-flex align-items-center">
        <div className="homemodal1-content shadow position-relative h-auto p-5">
          <button onClick={modalClose} className="modalclosebtn btn">
            &times;
          </button>
          <div className="row align-items-center h-100">
            <div className="col-md-6 ">
              <img
                className="modalbrandicon pb-3"
                src={modalValue && modalValue.icon}
                alt=""
                style={{
                  maxWidth:
                    modalValue && modalValue.maxWidth
                      ? modalValue.maxWidth
                      : "20%",
                }}
              />
              <h4>{modalValue && modalValue.heading}</h4>
            </div>
            <div className="col-md-6">
              <p className="textmodal textmodal1">
                {modalValue && modalValue.text1}
              </p>
              <p className="textmodal textmodal2">
                {modalValue && modalValue.text2}
              </p>
            </div>
          </div>{" "}
        </div>
      </div>
    </div>
  );
}
