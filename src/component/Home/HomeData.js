const HomeData = {
  Rich_NLP_Library: {
    heading: "Rich NLP Library",
    icon: "layer18",
    text1:
      "With AI based NLP you can automate complex test scenarios with comparatively 10x faster.",
    text2:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ",
  },
};
export default HomeData;
