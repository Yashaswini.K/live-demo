import React, { useState } from "react";
import "./Home.css";
import { Link } from "react-router-dom";
import carouselimg from "../../assets/carousel.svg";
import layer10 from "../../assets/home/layer10.png";
import HeaderComponent from "../HeaderComponent/HeaderComponent";
import Modal1 from "../ModalComponent/Home/Modal1";
import layer15 from "../../assets/home/layer15.png";
import layer18 from "../../assets/home/layer18.png";
import layer19 from "../../assets/home/layer19.png";
import layer41 from "../../assets/home/layer41.png";
import imgcircle from "../../assets/home/imgcircle.png";
import layer42 from "../../assets/home/layer42.png";
import FooterComponent from "../FooterComponent/FooterComponent";
import play from "../../assets/Footer/play.png";

import icon1 from "../../assets/home/icon1.png";
import icon2 from "../../assets/home/icon2.png";
import icon3 from "../../assets/home/icon3.png";
import icon4 from "../../assets/home/icon4.png";
import icon5 from "../../assets/home/icon5.png";
import icon6 from "../../assets/home/icon6.png";
import icon7 from "../../assets/home/icon7.png";
import icon8 from "../../assets/home/icon8.png";
import icon9 from "../../assets/home/icon9.png";
import icon10 from "../../assets/home/icon10.png";
import icon11 from "../../assets/home/icon11.png";

import chrome from "../../assets/home/chrome.png";
import sauce_labs from "../../assets/home/sauce_labs.png";
import aws from "../../assets/home/aws.png";
import azure from "../../assets/home/azure.png";
import cloud from "../../assets/home/cloud.png";
import jenkins from "../../assets/home/jenkins.png";
import jira from "../../assets/home/jira.png";
import atlassian from "../../assets/home/atlassian.png";
import codeship from "../../assets/home/codeship.png";
import slack from "../../assets/home/slack.png";
import hp from "../../assets/home/hp.png";

function Home({ activePage }) {
  const [modal, setModal] = useState(false);
  const [modalValue, setModalValue] = useState({
    heading: "",
    icon: "",
    text1: "",
    text2: "",
    maxWidth: "",
  });
  const Rich_NLP_Library = {
    heading: "Rich NLP Library",
    icon: layer18,
    text1:
      "With AI based NLP you can automate complex test scenarios with comparatively 10x faster.",
    text2:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ",
  };
  const Agile_adaptability = {
    heading: "Agile adaptability",
    icon: layer15,
    text1:
      "With AI based NLP you can automate complex test scenarios with comparatively 10x faster.",
    text2:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ",
  };
  const Rich_Analytics = {
    heading: "Rich NLP Library",
    icon: layer18,
    text1:
      "With AI based NLP you can automate complex test scenarios with comparatively 10x faster.",
    text2:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ",
  };

  //circle graph section constants starts
  const Self_Correction = {
    heading: "Self-Correction",
    icon: layer18,
    text1:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ",
  };

  const Script_versioning = {
    heading: "Script-versioning",
    icon: layer18,
    text1:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ",
  };
  const Optimizer = {
    heading: "Optimizer",
    icon: layer18,
    text1:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ",
  };
  const Core_Repo = {
    heading: "Core-Repo",
    icon: layer18,
    text1:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ",
  };
  const Rich_Report = {
    heading: "Rich-Report",
    icon: layer18,
    text1:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ",
  };
  const BDD_TDD = {
    heading: "BDD  & TDD",
    icon: layer18,
    text1:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ",
  };
  const In_built_Scheduler = {
    heading: "In built Scheduler",
    icon: layer18,
    text1:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ",
  };
  const SFD_CSAP = {
    heading: "SFDC & SAP",
    icon: layer18,
    text1:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ",
  };
  //circle graph section constants starts

  //brand section constants starts
  const CHROME = {
    heading: "Browser Stack",
    icon: chrome,
    text1:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ",
  };
  const SAUCE_LABS = {
    heading: "Sauce Labs",
    icon: sauce_labs,
    text1:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ",
  };
  const AWS = {
    heading: "AWS",
    icon: aws,
    text1:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ",
  };
  const AZURE = {
    heading: "Azure",
    icon: azure,
    text1:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ",
  };
  const CLOUD = {
    heading: "Cloud",
    icon: cloud,
    text1:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ",
  };
  const JENKINS = {
    heading: "Jenkins",
    icon: jenkins,
    text1:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ",
  };
  const JIRA = {
    heading: "Jira",
    icon: jira,
    text1:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ",
  };
  const ATLASSIAN = {
    heading: "Atlassian",
    icon: atlassian,
    text1:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ",
  };
  const Codeship = {
    heading: "Codeship",
    icon: codeship,
    text1:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ",
  };
  const Slack = {
    heading: "Slack",
    icon: slack,
    text1:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ",
  };
  const Hp = {
    heading: "HP",
    icon: hp,
    text1:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ",
  };
  //brand section constants ends

  const Rich_Analytics_Div = {
    heading: "Rich Analytics",
    icon: layer42,
    text1:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ",
    maxWidth: "70%",
  };

  const Reinforce = {
    heading: "Reinforce Continuous Integration to your DevOps process",
    icon: layer41,
    text1:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ",
    maxWidth: "70%",
  };

  let modalClose = () => {
    setModal(false);
  };

  const handleModal = (value) => {
    setModalValue({
      heading: value.heading,
      icon: value.icon,
      text1: value.text1,
      text2: value.text2,
      maxWidth: value.maxWidth,
    });
    setModal(true);
  };
  return (
    <div style={{ overflowX: "hidden" }}>
      <Modal1 modalClose={modalClose} modalValue={modalValue} modal={modal} />

      {/* main section  starts */}
      <div className="header_bg " style={{ position: "relative" }}>
        <div className="">
          <HeaderComponent activePage={activePage} />
          <div className=" ">
            <div className="row ">
              <div className="col-lg-6 mt-2">
                <div className=" mt-5">
                  <h1
                    style={{ textAlign: "left" }}
                    className="text-light homeh1 mx-5"
                  >
                    Test Automation. Redefined.
                  </h1>

                  <div style={{ textAlign: "left" }} className="textbg">
                    <p className="text-light mx-5">
                      Scriptless solutions to all your functional, API &
                      Database tests
                    </p>
                    <div className="row align-items-center mt-5 mx-5">
                      <div className=" col-6 col-md-6 col-lg-6  h-100 ">
                        <button className="btn  reqdemobtn ">
                          <Link
                            className=""
                            to="/requestdemo"
                            style={{ textDecoration: "none" }}
                          >
                            <p className="m-0 text-white requestdemotext px-2 py-1">
                              {" "}
                              REQUEST A DEMO
                            </p>
                          </Link>
                        </button>
                      </div>
                      <div className="col-6 col-md-6  col-lg-6  h-100 ">
                        <img
                          src={play}
                          className="homePlayBtn d-inline"
                          alt=""
                        />
                        <h6 className="homePlayText d-inline text-white">
                          WATCH THE VIDEO
                        </h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-6  mt-2 carouseldiv px-0 ">
                <Modal1 />
                <div className="mt-2 my-3  carouselchildiv">
                  {/* <div className="h-100 w-100 position-absolute top-0  pb-1">
                  <img className="imagebg " src={rightimagebg} />
                </div> */}
                  {/* <div className="h-100 w-100 position-absolute top-0  pb-1"> */}
                  <img
                    style={{
                      maxHeight: "100%",
                      width: "75%",
                      marginBottom: "10px",
                    }}
                    src={carouselimg}
                  ></img>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          style={{ position: "absolute", borderRadius: "10px", top: "96%" }}
          className="col-8 offset-2  bg-white my-0 rounded_div"
        >
          <h1
            style={{ fontSize: "20px", color: "#16378A" }}
            className="py-3 px-2 my-0"
          >
            Single Thread Multi Channel Execution
          </h1>
        </div>
      </div>
      {/* main section  ends */}

      {/* second seperation starts */}
      <div className="bg-white" style={{ height: "auto", width: "100%" }}>
        <div className="diamondtopdiv">
          <div className="d-flex justify-content-center  layer10div w-100">
            <img className="w-50 mx-auto mb-4" src={layer10}></img>
          </div>
          <div
            style={{
              borderRadius: "10px",
            }}
            className="col-8 offset-2   bg-white mt-5 rounded_div "
          >
            <h1
              style={{ fontSize: "20px", color: "#16378A" }}
              className="py-3 p-2"
            >
              Would you love know what makes us unique?
            </h1>
          </div>
        </div>

        <div className="diamondbottomdiv  ">
          <div className="row  pb-5 justify-content-between mx-2">
            <div
              className="card shadow col-12 mx-auto col-md-4  my-5 customcard"
              style={{ maxWidth: "18rem", border: "none" }}
            >
              <div className="card-body ">
                <div style={{ minHeight: "80px " }}>
                  <img className="w-25 my-2" src={layer18}></img>
                </div>
                <h5 className="card-title">Rich NLP Library</h5>
                <p className="card-text homecardtext">
                  With AI based Nlp you can automate complex test scenarios with
                  comparatively 10x faster
                </p>
                <div
                  className="btngradient rounded-pill w-75 mx-auto py-2 text-white"
                  onClick={() => {
                    handleModal(Rich_NLP_Library);
                  }}
                >
                  Read More {">"}
                </div>
              </div>
            </div>

            <div
              className="card shadow col-12 col-md-4 mx-auto my-5 customcard"
              style={{ maxWidth: "18rem", border: "none" }}
            >
              <div className="card-body">
                <div style={{ minHeight: "80px" }}>
                  <img className="w-25 my-2" src={layer15}></img>
                </div>
                <h5 className="card-title">Agile adaptability</h5>
                <p className="card-text homecardtext">
                  With AI based Nlp you can automate complex test scenarios with
                  comparatively 10x faster
                </p>
                <div
                  onClick={() => {
                    handleModal(Agile_adaptability);
                  }}
                  className="btngradient rounded-pill w-75 mx-auto py-2 text-white"
                >
                  Read More {">"}
                </div>
              </div>
            </div>
            <div
              className="card shadow col-12 col-md-4 mx-auto   my-5 customcard"
              style={{ maxWidth: "18rem", border: "none" }}
            >
              <div className="card-body">
                <div style={{ minHeight: "80px " }}>
                  <img className="w-25 my-2" src={layer19}></img>
                </div>
                <h5 className="card-title">Rich Analytics</h5>
                <p className="card-text homecardtext">
                  Optimize your elements locators Test Productivity analytics
                  Execution efficiency analytics Test Result analytics
                </p>
                <div
                  onClick={() => {
                    handleModal(Rich_Analytics);
                  }}
                  className="btngradient rounded-pill w-75 mx-auto py-2 text-white"
                >
                  Read More {">"}
                </div>
              </div>
            </div>
          </div>
          {/* circle section starts new */}
          <div className="circleSection pb-5">
            <div className="d-flex justify-content-around">
              <div
                onClick={() => {
                  handleModal(Self_Correction);
                }}
                className="mx-5 bg-white rounded m-1 rounded m-1 shadow "
                style={{ width: "auto", minWidth: "18%", whiteSpace: "nowrap" }}
              >
                <div
                  id="pointer"
                  className="p-2 text-white"
                  // onClick={selfCorrectionModal}
                >
                  Self Correction
                </div>
              </div>
              <div
                onClick={() => {
                  handleModal(Script_versioning);
                }}
                className="mx-5 bg-white  rounded  rounded m-1 shadow"
                style={{ width: "auto", minWidth: "18%", whiteSpace: "nowrap" }}
              >
                <div
                  id="pointer"
                  className="p-2 text-white"
                  // onClick={scriptversioningModal}
                >
                  {" "}
                  Script versioning
                </div>
              </div>
            </div>
            <div className="d-flex justify-content-center">
              <div
                className="d-flex flex-column justify-content-around "
                style={{ width: "25%" }}
              >
                <div
                  onClick={() => {
                    handleModal(Optimizer);
                  }}
                  className="mx-5  bg-white  rounded m-1 shadow"
                  style={{
                    width: "auto",
                    minWidth: "18%",
                    whiteSpace: "nowrap",
                  }}
                >
                  <div id="pointer" className="p-2 text-white">
                    {" "}
                    Optimizer
                  </div>
                </div>
                <div
                  onClick={() => {
                    handleModal(Rich_Report);
                  }}
                  className="mx-5 bg-white rounded  rounded m-1 shadow"
                  style={{
                    width: "auto",
                    minWidth: "18%",
                    whiteSpace: "nowrap",
                  }}
                >
                  <div id="pointer" className="p-2 text-white">
                    {" "}
                    Rich Report
                  </div>
                </div>
              </div>
              <div className="" style={{ width: "45%" }}>
                <div
                  className="circlediv mx-auto"
                  style={{
                    borderRadius: "50%",
                    // height: "400px",
                    // width: "400px",
                  }}
                >
                  <img
                    className="imgcircle "
                    style={{ margin: "11%" }}
                    src={imgcircle}
                  ></img>
                </div>
              </div>
              <div
                className="d-flex flex-column justify-content-around "
                style={{ width: "25%" }}
              >
                <div
                  onClick={() => {
                    handleModal(Core_Repo);
                  }}
                  className="mx-5 bg-white rounded m-1 shadow"
                  style={{
                    width: "auto",
                    minWidth: "18%",
                    whiteSpace: "nowrap",
                  }}
                >
                  <div id="pointer" className="p-2 text-white">
                    {" "}
                    Core Repo
                  </div>
                </div>
                <div
                  onClick={() => {
                    handleModal(BDD_TDD);
                  }}
                  className="mx-5 bg-white rounded m-1 rounded m-1 shadow"
                  style={{
                    width: "auto",
                    minWidth: "18%",
                    whiteSpace: "nowrap",
                  }}
                >
                  <div id="pointer" className="p-2 text-white">
                    {" "}
                    BDD & TDD
                  </div>
                </div>
              </div>
            </div>
            <div className="d-flex justify-content-around pb-5">
              <div
                onClick={() => {
                  handleModal(In_built_Scheduler);
                }}
                className="mx-5 bg-white rounded m-1 rounded m-1 shadow"
                style={{ width: "auto", minWidth: "18%", whiteSpace: "nowrap" }}
              >
                <div id="pointer" className="p-2 text-white">
                  {" "}
                  In built Scheduler
                </div>
              </div>
              <div
                onClick={() => {
                  handleModal(SFD_CSAP);
                }}
                className="mx-5 bg-white rounded m-1 rounded m-1 shadow"
                style={{ width: "auto", minWidth: "18%", whiteSpace: "nowrap" }}
              >
                <div id="pointer" className="p-2 text-white">
                  SFDC & SAP
                </div>
              </div>
            </div>
          </div>
          {/* circle section ends new */}
        </div>
      </div>
      {/* second seperation ends */}

      {/* third seperation start new */}
      <div className="brandDiv text-white row">
        <div className="col-12 col-md-7 col-lg-7 brandtexdiv">
          <div className="width80 mx-auto textLeft my-5 ">
            <div className="pt-5 pl-5 headingp">
              <p>Integrations</p>
            </div>
            <div className="pb-3 pl-5">
              <h1>Flexibly can fit to your environment</h1>
            </div>
            <div className="homep pl-5 pt-4">
              <p homebrandtext>
                {" "}
                Sample text is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book. It has
                survived not only five centuries, but also the leap into
                electronic typesetting, remaining essentially unchanged. It was
                popularised in the 1960s with the release of Letraset sheets
                containing Lorem Ipsum passages, and more recently with desktop
                publishing software like Aldus PageMaker including versions of
                Lorem Ipsum.
              </p>
            </div>
          </div>
        </div>
        <div className=" col-12 col-md-5 col-lg-5 h-50 my-auto">
          <div className=" mx-auto  d-flex justify-content-between homewidth75">
            <img
              className="iconMaxWidth"
              src={icon1}
              alt=""
              onClick={() => {
                handleModal(CHROME);
              }}
            />
            <img
              className="iconMaxWidth"
              src={icon2}
              alt=""
              onClick={() => {
                handleModal(SAUCE_LABS);
              }}
            />
            <img
              className="iconMaxWidth"
              src={icon3}
              alt=""
              onClick={() => {
                handleModal(AWS);
              }}
            />
          </div>
          <div className=" mx-auto d-flex justify-content-between homewidth75">
            <img
              className="iconMaxWidth"
              src={icon4}
              alt=""
              onClick={() => {
                handleModal(AZURE);
              }}
            />
            <img
              className="iconMaxWidth"
              src={icon5}
              alt=""
              onClick={() => {
                handleModal(CLOUD);
              }}
            />
            <img
              className="iconMaxWidth"
              src={icon6}
              alt=""
              onClick={() => {
                handleModal(JENKINS);
              }}
            />
          </div>
          <div className=" mx-auto d-flex justify-content-between homewidth75">
            <img
              className="iconMaxWidth"
              src={icon7}
              alt=""
              onClick={() => {
                handleModal(JIRA);
              }}
            />
            <img
              className="iconMaxWidth"
              style={{ padding: "7.5%" }}
              src={icon8}
              alt=""
              onClick={() => {
                handleModal(ATLASSIAN);
              }}
            />
            <img
              className="iconMaxWidth"
              src={icon9}
              alt=""
              onClick={() => {
                handleModal(Codeship);
              }}
            />
          </div>
          <div className="mx-auto mb-2  d-flex justify-content-between homewidth75">
            <img className="iconMaxWidth invisible" src={icon10} alt="" />
            <img
              className="iconMaxWidth"
              src={icon10}
              alt=""
              onClick={() => {
                handleModal(Slack);
              }}
            />
            <img
              className="iconMaxWidth"
              src={icon11}
              alt=""
              onClick={() => {
                handleModal(Hp);
              }}
            />
          </div>
        </div>
      </div>
      {/* third seperation ends new*/}

      {/* Fourth seperation start */}
      <div className="bg-white pt-3 my-3">
        <div className="row">
          <div className="col-md-6 col-12 homedivleft d-flex align-items-center">
            <div className="mx-auto " style={{ width: "80%" }}>
              <img style={{}} className="w-100 h-auto" src={layer42}></img>
            </div>
          </div>
          <div className="col-md-6 col-12 homedivright   text-left">
            <div className=" mt-5 width80 homedivrightchild">
              <h1
                className="mr-3"
                style={{ textAlign: "left", fontSize: "1.8em", width: "90%" }}
              >
                Reinforce Continuous Integration to your DevOps process{" "}
              </h1>
              <p className="my-4 w-100 homedivtextstyle">
                Integrating your UI tests to CI/CD pipeline is now simple.
                Inbuilt integration to Azure & AWS machines with quick setup
                configuration
              </p>
              <p className=" w-100 homedivtextstyle">
                Integrating your UI tests to CI/CD pipeline is now simple.
                Inbuilt integration to Azure & AWS machines with quick setup
                configuration
              </p>
              <div
                className="btngradient homecustombutton rounded-pill w-5 mx-auto py-2 text-white"
                onClick={() => {
                  handleModal(Reinforce);
                }}
              >
                Read More {">"}
              </div>
              {/* <button
                className="btn d-inline-block btngradient homecustombutton rounded-pill w-5 mx-auto py-2 text-white"
                onClick={() => {
                  handleModal(Reinforce);
                }}
              >
                Read More {">"}
              </button> */}
            </div>
          </div>
        </div>
      </div>
      <div className="bg-white pb-5 my-3">
        <div className="row">
          <div className="col-md-6 col-12 homedivleft1 pb-5 mt-5 ">
            <div className=" width80 mx-auto">
              <h1
                className="mr-3"
                style={{ textAlign: "left", fontSize: "1.8em", width: "90%" }}
              >
                Rich Analytics{" "}
              </h1>
              <p className="my-4 w-100 homedivtextstyle">
                Grades your engineers performance with its accurate reports
                Optimize your elements locators Test Productivity analytics
              </p>
              <p className=" w-100 pb-3 homedivtextstyle">
                Integrating your UI tests to CI/CD pipeline is now simple.
                Inbuilt integration to Azure & AWS machines with quick setup
                configuration
              </p>
              <div
                className="btngradient homecustombutton rounded-pill w-5 mx-auto py-2 text-white"
                onClick={() => {
                  handleModal(Rich_Analytics_Div);
                }}
              >
                Read More {">"}
              </div>
              {/* <button
                className="btn btngradient homecustombutton homecustombutton  rounded-pill w-5 mx-auto py-2 text-white "
                onClick={() => {
                  handleModal(Rich_Analytics_Div);
                }}
              >
                Read More {">"}
              </button> */}
            </div>
          </div>
          <div className="col-md-6 col-12  d-flex align-items-center pr-3">
            <div className="mx-auto " style={{ width: "80%" }}>
              <img style={{}} className="w-100 h-auto" src={layer41}></img>
            </div>
          </div>
        </div>
      </div>
      {/* Fourth seperation ends */}
      <FooterComponent background={"#7171E3"} image={false} />
    </div>
  );
}

export default Home;
