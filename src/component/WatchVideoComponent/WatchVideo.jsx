import React from "react";
import play from "../../assets/Footer/play.png";
import watchvideo from "../../assets/Footer/watchvideo.png";

export default function WatchVideo() {
  return (
    <div>
      <div className="col-lg-3">
        <span className="col-lg-1">
          <img src={play} className="footerplay" alt="" />
        </span>
        <span className="col-lg-2">
          <img src={watchvideo} className="footerwatch" alt="" />
        </span>
      </div>
    </div>
  );
}
